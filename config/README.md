Répertoire "config" :
------------------------

- Contient les dossiers de configuration des machines virtuelles.
- Contient les variables globales.


Structure :
-----------

Dossier vm_all : Contient la configuration commune à toutes les machines virtuelles.

Dossiers vm_0 : Contient un exemple de configuration de machine virtuelle sans service autre que les services listés dans les configurations vm_all.

Dossier vm_1, vm_2, vm_NUM... : Contient la configuration de la machine virtuelle numérotée.



Fichiers de configurations :
----------------------------

Chaque répertoire de configuration de machine virtuelle contient deux fichiers.

config.sh : Contient les variables globales de tous les services installés sur cette machine à l'exception des variables sensibles.

private.sh : Contient les variables sensibles (port ssh, informations secretes). Ce fichier contient de base la liste des variables globales secretes avec des valeurs de test. Changez ces valeurs juste avant de lancer l'installation sur la machine virtuelle.



# Strucuture des VM :

Chaques VM aura une utilité définie. Voici la liste des vm avec leur utilité :

- vm_0	OpenVPN
- vm_1	LDAP server
- vm_2	WebAdmin LDAP
- vm_3	Yunohost
- vm_4	
- vm_5	
- vm_6	
- vm_7	- openvpn
