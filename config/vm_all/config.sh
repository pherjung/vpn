#!/bin/bash

#############################################################
# Définitions des variables globales communes à toutes les vm
#############################################################


####################
# Names
####################

ASSOS_NAME="swissneutralnet"
ASSOS_EMAIL="info@swissneutral.net"

# Main
MAIN_DOMAIN="swissneutral.net"
INFRA_DOMAIN="infra.$MAIN_DOMAIN"

# Nommage des services
LDAP_SERVER_NAME="ldap"
DNS_SERVER_1_NAME="tom"
DNS_SERVER_2_NAME="jerry"

LDAP_SERVER_FQDN="$LDAP_SERVER_NAME.$INFRA_DOMAIN"
DNS_SERVER_1_FQDN="$DNS_SERVER_1_NAME.$INFRA_DOMAIN"
DNS_SERVER_2_FQDN="$DNS_SERVER_2_NAME.$INFRA_DOMAIN"

####################
# Network
####################

# SNN Global adressing
SNN_PUBLIC_IPV4_NET="10.10.0.0"
SNN_PUBLIC_IPV4_NETMASK_CIDR="16"
SNN_PUBLIC_IPV6_PREFIX="2a02:121e:251f:e00"
SNN_PUBLIC_IPV6_NET="${SNN_PUBLIC_IPV6_PREFIX}0::"
SNN_PUBLIC_IPV6_NETMASK="62"

# DNS servers
DNS_SERVER_1_IP4="10.10.20.137"
DNS_SERVER_2_IP4="10.10.20.137"
DNS_SERVER_1_IP6="${SNN_PUBLIC_IPV6_PREFIX}1:20::137"
DNS_SERVER_2_IP6="${SNN_PUBLIC_IPV6_PREFIX}1:20::137"
LDAP_SERVER="10.10.30.10"


####################
# Main services
####################

# Services to install on each vm
ROOT_SERVICES="time ssh firewall security_update fail2ban"

####################
# Time
####################
TIME_ZONE="Europe/Zurich"

####################
# OpenVPN
####################

VPN_1_SERVER_IP4="10.10.50.0"
VPN_1_NETMASQ_IP4="255.255.255.192"
VPN_1_SERVER_IP6="${SNN_PUBLIC_IPV6_PREFIX}2:a::"
VPN_1_NETMASQ_IP6="80"

OPENVPN_SERVER1_CONF_FOLDER="/etc/openvpn"
OPENVPN_SERVER1_CERT_FOLDER="$OPENVPN_SERVER1_CONF_FOLDER/cert"

OPENVPN_SERVER1_CA="$OPENVPN_SERVER1_CERT_FOLDER/ca.crt"		# CA certificat location
OPENVPN_SERVER1_CERT="$OPENVPN_SERVER1_CERT_FOLDER/server.crt"		# server certificat location
OPENVPN_SERVER1_KEY="$OPENVPN_SERVER1_CERT_FOLDER/server.key"		# server key location
OPENVPN_SERVER1_DH="$OPENVPN_SERVER1_CERT_FOLDER/dh.pem"		# server DH key location
OPENVPN_SERVER1_TA="$OPENVPN_SERVER1_CERT_FOLDER/ta.key"	# server tls ta key location

####################
# LDAP service
####################

# Services to install on each vm


####################
# OpenVPN - CA
####################
# CA-Swissneutral.net
OPENVPN_CA_DEFAULT_BITS="4096"

OPENVPN_CA_COUNTRY="CH"
OPENVPN_CA_STATE="VD"
OPENVPN_CA_LOCALITY="Aigle"
OPENVPN_CA_ORGANISATION="SwissNeutral.net"
OPENVPN_CA_COMMON="SNN VPN SERVER CA"
OPENVPN_CA_FQDN="ca.vpn.swissneutral.net"

####################
# ssh
####################
SSH_KEY_LENGTH="4096"
SSH_PRIVATE_KEY_LOCTION="/root/.ssh/id_rsa"
ADMINS_KEYS_LOCATION="/root/.ssh/id_rsa"
SSH_AUTHORIZED_KEYS_LOCTION="/root/.ssh/authorized_keys"

####################
# security update
####################
REPORT_MAIL='root'

####################
#DNS master/ slave
####################
DNS_MASTER_NAME="Laurel"
DNS_MASTER_FQDN="$DNS_MASTER_NAME.$INFRA_DOMAIN"
DNS_MASTER_IP4="127.0.0.1"
DNS_MASTER_IP6="::1"

DNS_SLAVE_1_NAME="Hardy"
DNS_SLAVE_1_FQDN="$DNS_SLAVE_1_NAME.$INFRA_DOMAIN"
DNS_SLAVE_1_IP4="127.0.0.1"
DNS_SLAVE_1_IP6="::1"

####################
# fail2ban
####################
FAIL2BAN_IGNOREIP="127.0.0.1/8"
FAIL2BAN_BANTIME="604800"
FAIL2BAN_FINDTIME="86400"
FAIL2BAN_MAXRETRY="5"


