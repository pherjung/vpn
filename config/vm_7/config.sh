#!/bin/bash

###################################################################
# Définitions des variables globales propres à la machine virtuelle
###################################################################

# Services supplémentaires à installer sur cette vm
LOCAL_SERVICES="resolver"
FIREWALL_UDP_PORT="53"
FIREWALL_TCP_PORT="53"
