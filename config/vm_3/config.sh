#!/bin/bash

###################################################################
# Définitions des variables globales propres à la machine virtuelle
###################################################################

# Services supplémentaires à installer sur cette vm
LOCAL_SERVICES="yunohost"
ROOT_SERVICES="time ssh security_update"
YUNOHOST_DOMAIN="yunohost.$MAIN_DOMAIN"
YUNOHOST_UPDATE_MAIL="josue@swissneutral.net florian@swissneutral.net"
REPORT_MAIL=""