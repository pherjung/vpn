#!/bin/bash

###################################################################
# Définitions des variables globales propres à la machine virtuelle
###################################################################

# Services supplémentaires à installer sur cette vm
LOCAL_SERVICES="openvpn openvpn-cert"
REPORT_MAIL="root"
FIREWALL_UDP_PORT="1194 1195"
FIREWALL_TCP_PORT="443"
WAN_INTERFACE="eth0"
EASYRSA_VERSION="3.0.4"