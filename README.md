Installation de la machine virtuelle N°NUM :
------------------------
En root sur la machine virtuelle:

```
apt-get install git-core -y
git clone https://framagit.org/swissneutral.net/vpn.git
cd vpn/

./main.sh INSTALL NUM	# Replace NUM by the vm_NUM folder number
```

OU

Sur son ordinateur local:

```
git clone https://framagit.org/swissneutral.net/vpn.git
cd vpn/

#Editer le fichier remote_install.sh

./remote_install.sh
```

# Explications des repos :

La structure de ce dépot est définie comme suit :

Répertoire "backup" :
------------------------

Répertoire où seront créées les sauvegardes des différents services d'une machines virtuelle.


Répertoire "config" :
------------------------

- Contient les dossiers de configuration des machines virtuelles.
- Contient les variables globales.


Répertoire "doc" :
------------------------

Contient de la documentation.


Répertoire "services" :
------------------------

- Contient les différents services qui peuvent être installé sur les machines virtuelles.
- Contient les fichiers nécéssaires à l'installation des services.
- Contient les scripts de déployements des différents services.


Fichier "main.sh" :
------------------------

Script principal d'installation des services sur une vm.

Utilisation:
./main.sh CMD NUM


CMD: commande a executer pour les services de cette machine :

- INSTALL	- Installe les services listés dans le fichier config/vm_NUM/config.sh
- UPDATE	- Met à jour les services sur une machine en fonctionnement
- BACKUP	- Sauvegarde la configuration des services dans le répertoire backup
- RESTORE	- Restore une configuration du répertoire backup sur une machine en service


NUM: numéro du dossier de configuration de la machine virtuelle - config/vm_NUM

Exemple dossier vm_7 :
./main.sh CMD 7


Fichier "remote_install.sh" :
------------------------

A utiliser sur son ordinateur local. Copie le répertoire vpn/ vers la machine virtuelle et lance le scripte main.sh.

A utiliser pour des tests lors de développements de services.

A utiliser pour l'installation a distance depuis une machine locale.

Ouvrir le fichier et changez les valeurs internes avant d'exécuter. (Provisoire)


# Structure des VM :

Chaques VM aura une utilité définie. Voici la liste des vm avec leur utilité :

- vm_0	- EXEMPLE - Copiez cette configuration dans un nouveau répertoire vm_NUM pour créer une nouvelle machine sans service supplémentaire.

- vm_1	- firewall test
- vm_2	- ldap test


# Structure des services :

Chaques services est définit dans le répertoire "services".
Il est nécessaire de créer un nouveau répertoire pour chaque service comprenant un script d'installation.
Voici la liste des différents services sous la forme "service_name - service_function"

Fonctionnels:


En cours de construction:

- openvpn	- OpenVPN Server
- ldap		- LDAP Server
- ssh		- OpenSSH server
- firewall	- iptables, ip6tables
- bind		- Domaine Name Server


A faire:

- Résolveur DNS 1
- Résolveur DNS 2
- DNS autoritaire master (pour la zone swissneutral.net)
- DNS autoritaire slave
- Serveur web AlternC
- Mailing-list
- Git
- Mumble
