#!/bin/bash

##################################################
# Service - exemple
#
# This is an exemple of service script file
##################################################
cd "$( dirname "${BASH_SOURCE[0]}" )"

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
web_root="/var/www/html"

####################
# Install
####################

PREINSTALL()
{
	apt-get install nginx -y
}

POSTINSTALL()
{
	#Remove default vhost
	rm -f /etc/nginx/sites-enabled/default
	
	service nginx restart
}


####################
# Update
####################

PREUPDATE()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

POSTUPDATE()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	cd $web_root
	tar -czf $backup_path/www_bk.tar.gz .
}
	

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	if [ ! -d $web_root ]
	then
		mkdir -p $web_root
	fi

	cd $restore_path
	tar -xzvf www_bk.tar.gz -C $web_root
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
