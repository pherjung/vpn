#!/usr/bin/env bash

##################################################
# Service - unbound
#
# This is an exemple of service script file
##################################################

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')


# Variables
unbound_files="/etc/unbound"


####################
# Install
####################

PREINSTALL()
{
    apt-get install curl unbound dnsutils -y unbound-host dnssec-trigger


    mkdir -p $unbound_files

    curl -s -L https://internic.net/domain/named.cache -o $unbound_files/root.hints
    md5_sum=$(curl -L -s http://internic.net/domain/named.cache.md5)
    md5_file=$(md5sum  $unbound_files/root.hints |awk '{print $1}')

    if [ "$md5_sum" != "$md5_file" ]
    then
        echo "Hash check failed. Delete file $unbound_files/root.hints"
        rm -rf $unbound_files/root.hints
    fi

    mkdir -p /var/log/unbound
    chown unbound -R /var/log/unbound

}

POSTINSTALL()
{
   service unbound restart
}


####################
# Update
####################

PREUPDATE()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


POSTUPDATE()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
