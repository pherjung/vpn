#!/bin/bash

##################################################
# Service - ssh
##################################################

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
local_folder=`dirname $(readlink -f $0)`


####################
# Local variables
####################

admins_keys_location="data/admin_keys/"


####################
# Admins public keys
####################

add_admins_keys()
{
	# Add admins rsa public keys
	for admin_name in $ADMINS_LIST
	do
		cat $local_folder/$admins_keys_location$admin_name.pub >> $SSH_AUTHORIZED_KEYS_LOCTION
	done
}


####################
# Install
####################

PREINSTALL()
{
	apt-get install -y openssh-server
}

POSTINSTALL()
{
	# Generate rsa keys
	ssh-keygen -b $SSH_KEY_LENGTH -t rsa -f $SSH_PRIVATE_KEY_LOCTION -q -N ""

	add_admins_keys

	# Restart service
	service ssh restart
}


####################
# Update
####################

PREUPDATE()
{
	:
}

POSTUPDATE()
{

	# Remove old admins keys
	rm $SSH_AUTHORIZED_KEYS_LOCTION

	# Add updated admins keys
	add_admins_keys

	# Restart service
	service ssh restart
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
