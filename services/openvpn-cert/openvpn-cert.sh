#!/bin/bash

##################################################
# Service - openvpn certification
#
# This is an exemple of service script file
##################################################

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')

####################
# Install
####################

PREINSTALL()
{
	apt-get install openssl -y
	wget https://github.com/OpenVPN/easy-rsa/releases/download/v$EASYRSA_VERSION/EasyRSA-$EASYRSA_VERSION.tgz
	tar -xzf EasyRSA-$EASYRSA_VERSION.tgz
	rm EasyRSA-$EASYRSA_VERSION.tgz
	mv EasyRSA-$EASYRSA_VERSION /opt
}

POSTINSTALL() {
    echo "You need to build manually the certificate"
}


####################
# Update
####################

PREUPDATE()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

POSTUPDATE()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
