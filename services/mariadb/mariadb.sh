#!/usr/bin/env bash
##################################################
# Service - mariadb
#
##################################################


# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')
password_file_root="$HOME/mysql_password_root"

gen_pass(){
password_file=$1

password_gen="$(date +%s | sha256sum | base64 | head -c 32)"

if [ ! -f $password_file ]
        then
                echo $password_gen > $password_file
                chmod 400 $password_file
        fi
}

####################
# Install
####################

PREINSTALL()
{

        #Generate password
        gen_pass $password_file_root
        password="$(cat $password_file_root)"

        #Install with no prompt
        export DEBIAN_FRONTEND=noninteractive
        debconf-set-selections <<< "mariadb-server mysql-server/root_password password $password"
        debconf-set-selections <<< "mariadb-server mysql-server/root_password_again password $password"
        apt-get install mariadb-server -y
}

POSTINSTALL()
{

        cd "$( dirname "${BASH_SOURCE[0]}" )" 
        #Generate password
        local password_file_adminSNN="$HOME/mysql_password_adminSNN"
        password_gen="$(date +%s | sha256sum | base64 | head -c 32)"

        if [ ! -f $password_file_adminSNN ]
        then
                echo $password_gen > $password_file_adminSNN
                chmod 400 $password_file_adminSNN
        fi

        password="$(cat $password_file_adminSNN)"
        sed -i "s@__PASSWORD__@$password@g" data/config.sql

        password_root="$(cat $password_file_root)"
        mysql -u root -p$password_root < data/config.sql
}

####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

        # Remove this line when implemented
        echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

        # Remove this line when implemented
        echo "Not implemented  $FUNCNAME in $service_name"
}

####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
        POSTINSTALL
    ;;
    PREUPDATE)
        PREUPDATE
    ;;
    POSTUPDATE)
        POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
        echo "
****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
