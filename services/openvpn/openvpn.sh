#!/bin/bash

##################################################
# Service - openvpn
#
# This is an exemple of service script file
##################################################

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')

####################
# Install
####################
PREINSTALL()
{
	apt-get install -y openvpn openvpn-auth-ldap sudo
	mkdir -p $OPENVPN_SERVER1_CERT_FOLDER
# 	openssl dhparam -out $OPENVPN_SERVER1_CERT_FOLDER/dh.pem 2048
# 	cp -r /usr/share/easy-rsa /root/openvpn-certs # on copie les fichier de base dans un repertoir de travail (au chaud des regard indiscrets)
    mkdir /etc/openvpn/ccd
}

POSTINSTALL()
{
	useradd openvpn
	mkdir -p /var/log/openvpn
	chown openvpn: /var/log/openvpn
	chown openvpn: -R /etc/openvpn
	chmod u=rwX,g=rX,o= /var/log/openvpn
	chmod u=rwX,g=rX,o= /etc/openvpn

# 	generateServerCertRequest
    
    echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
    echo "net.ipv6.conf.all.forwarding=1" >> /etc/sysctl.conf
    
    # Create iptables rules
    iptables -A FORWARD -i tun1 -o $WAN_INTERFACE -j ACCEPT
    iptables -A FORWARD -i tun2 -o $WAN_INTERFACE -j ACCEPT
    iptables -A FORWARD -i tun3 -o $WAN_INTERFACE -j ACCEPT
    iptables -A FORWARD -i $WAN_INTERFACE -o tun1 -j ACCEPT
    iptables -A FORWARD -i $WAN_INTERFACE -o tun2 -j ACCEPT
    iptables -A FORWARD -i $WAN_INTERFACE -o tun3 -j ACCEPT
    ip6tables -A FORWARD -i tun1 -o $WAN_INTERFACE -j ACCEPT
    ip6tables -A FORWARD -i tun2 -o $WAN_INTERFACE -j ACCEPT
    ip6tables -A FORWARD -i tun3 -o $WAN_INTERFACE -j ACCEPT
    ip6tables -A FORWARD -i $WAN_INTERFACE -o tun1 -j ACCEPT
    ip6tables -A FORWARD -i $WAN_INTERFACE -o tun2 -j ACCEPT
    ip6tables -A FORWARD -i $WAN_INTERFACE -o tun3 -j ACCEPT
    
    # configure SUDO
    echo "openvpn	ALL=(root) NOPASSWD:/bin/ip route *, /bin/ip -6 route *" >> /etc/sudoers
    
    echo " " >> /etc/openvpn/auth/ldap.conf # Hack pour résoudre l'erreur "segfault"
    
    
    # Hack Permet de démarrer openvpn correctement 
    sed -i "s/CapabilityBoundingSet=CAP_IPC_LOCK CAP_N/#CapabilityBoundingSet=CAP_IPC_LOCK CAP_N/g" /lib/systemd/system/openvpn@.service
	sed -i "s/LimitNPROC=10/#LimitNPROC=10/g" /lib/systemd/system/openvpn@.service
	
	systemctl daemon-reload
	systemctl restart openvpn
}


####################
# Update
####################

PREUPDATE()
{
	:
}

POSTUPDATE()
{
	updateVpnConfig
	service openvpn restart
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
