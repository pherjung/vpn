#!/bin/bash

retourn_update=$(yunohost --verbose tools update)

if [[ "$retourn_update" !=  *"There is no package to upgrade"* ]]
then
	if [[ "$(cat /tmp/send_mail_update.txt)" != "$retourn_update" ]]
	then
		echo "$retourn_update" > /tmp/send_mail_update.txt
		chmod 600 /tmp/send_mail_update.txt
		echo "$retourn_update" | mail -s "Nouvelles Mises à jours Yunohost" __YUNOHOST_UPDATE_MAIL__
	fi
fi

exit 0
