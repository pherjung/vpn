#!/bin/bash

##################################################
# Service - bind
#
# This is an exemple of service script file
##################################################

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')


# Variables
bind_folder="/etc/bind"



####################
# Install
####################

PREINSTALL()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"

	# Installation de Bind9
#	apt-get install bind9 -y

	# Suppression des fichiers de configuration de base
#	rm -r $bind_folder
}

POSTINSTALL()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"

	# Installation des fichiers de configuration
#	cp /root/git/infra/root1/etc/bind/* .

	# Redémarrage de bind
#	/etc/init.d/bind9 restart
}


####################
# Update
####################

PREUPDATE()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


POSTUPDATE()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"

	# Redémarrage de bind
#	/etc/init.d/bind9 restart
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
