#!/bin/bash

##################################################
# Service - ldap
#
# This is an exemple of service script file
##################################################


# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')


####################
# Install
####################

PREINSTALL()
{
	apt-get install -y phpldapadmin
}

POSTINSTALL()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Update
####################

PREUPDATE()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

POSTUPDATE()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}

RESTORE()
{
	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP
    ;;
    RESTORE)
        RESTORE
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
