#!/bin/bash

echo "Init ip6tables"

# Get all installed services on this VM
services_to_allow="__ROOT_SERVICES__ __LOCAL_SERVICES__"

#############################################################
#			FLUSH
#############################################################
ip6tables -P INPUT ACCEPT
ip6tables -P FORWARD ACCEPT
ip6tables -P OUTPUT ACCEPT

ip6tables -F INPUT
ip6tables -F FORWARD
ip6tables -F OUTPUT


#############################################################
#			INPUT
#############################################################

####################
# Already established
####################
ip6tables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT


####################
# Loopback
####################
ip6tables -A INPUT -i lo -j ACCEPT


####################
# ICMP
####################
ip6tables -A INPUT -p ipv6-icmp -j ACCEPT


####################
# Link-local
####################
ip6tables -A INPUT -s fe80::/10 -j ACCEPT


####################
# Multicast
####################
ip6tables -A INPUT -d ff00::/8 -j ACCEPT


####################
# Services
####################
# Scan all services installed on the VM
for service in $services_to_allow; do

	# Allow port for each specific service
	case "$service" in
	    ssh)
	        ip6tables -A INPUT -p tcp --dport __SSH_PORT__ -j ACCEPT
	    ;;
	    apache)
	        ip6tables -A INPUT -p tcp --dport 80 -j ACCEPT
		ip6tables -A INPUT -p tcp --dport 443 -j ACCEPT
	    ;;
	    *)
		# Do nothing if service don't need network
	        :
	    ;;
	esac
done

for port in $FIREWALL_TCP_PORT
do
    ip6tables -A INPUT -p tcp --dport $port -j ACCEPT
done

for port in $FIREWALL_UDP_PORT
do
    ip6tables -A INPUT -p udp --dport $port -j ACCEPT
done

####################
# RH0 packets
####################
ip6tables -A INPUT -m rt --rt-type 0 -j DROP


####################
# Default policy
####################
ip6tables -P INPUT DROP

#############################################################
#			FORWARD
#############################################################

####################
# Default policy
####################
ip6tables -P FORWARD DROP


#############################################################
#			OUTPUT
#############################################################

####################
# Default policy
####################
ip6tables -P OUTPUT ACCEPT

exit 0
