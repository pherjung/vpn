#!/bin/bash

# IPv4 only
echo "Init iptables"

# Get all installed services on this VM
services_to_allow="__ROOT_SERVICES__ __LOCAL_SERVICES__"

#############################################################
#			FLUSH
#############################################################
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT

iptables -F INPUT
iptables -F FORWARD
iptables -F OUTPUT


#############################################################
#			INPUT
#############################################################

####################
# Already established
####################
iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT


####################
# Loopback
####################
iptables -A INPUT -i lo -j ACCEPT


####################
# ICMP
####################
iptables -A INPUT -p icmp -j ACCEPT


####################
# Services
####################
# Scan all services installed on the VM
for service in $services_to_allow; do

	# Allow port for each specific service
	case "$service" in
	    ssh)
	        iptables -A INPUT -p tcp --dport __SSH_PORT__ -j ACCEPT
	    ;;
	    apache)
	        iptables -A INPUT -p tcp --dport 80 -j ACCEPT
		iptables -A INPUT -p tcp --dport 443 -j ACCEPT
	    ;;
	    *)
		# Do nothing if service don't need network
	        :
	    ;;
	esac
done

for port in $FIREWALL_TCP_PORT
do
    iptables -A INPUT -p tcp --dport $port -j ACCEPT
done

for port in $FIREWALL_UDP_PORT
do
    iptables -A INPUT -p udp --dport $port -j ACCEPT
done

####################
# Default policy
####################
iptables -P INPUT DROP

#############################################################
#			FORWARD
#############################################################

####################
# Default policy
####################
iptables -P FORWARD DROP


#############################################################
#			OUTPUT
#############################################################

####################
# Default policy
####################
iptables -P OUTPUT ACCEPT

exit 0
