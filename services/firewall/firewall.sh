#!/bin/bash

##################################################
# Service - firewall
#
# This is an exemple of service script file
##################################################

# Get service name from file name
service_name=$(echo `basename "$0"` | cut -f 1 -d '.')


####################
# Run config script file
####################
run_config_script()
{
	`dirname $0`/data/init_iptables.sh
	`dirname $0`/data/init_ip6tables.sh
}


####################
# Install iptables-persistent
####################
install_iptables_persistent()
{
	# Set default install parameters for iptables-persistent
	echo "iptables-persistent iptables-persistent/autosave_v4 boolean false" | debconf-set-selections
	echo "iptables-persistent iptables-persistent/autosave_v6 boolean false" | debconf-set-selections

	# Install iptables-persistent
	apt-get install iptables-persistent -y
}


####################
# Make iptable persistent
####################
save_current_iptables()
{
	# Save for iptables-persistent
	iptables-save > /etc/iptables/rules.v4
	ip6tables-save > /etc/iptables/rules.v6
}


####################
# Install
####################

PREINSTALL()
{
	# Install iptable-persistent
	install_iptables_persistent
}

POSTINSTALL()
{
	# Run config script file
	run_config_script

	# Make iptables persistent
	save_current_iptables
}


####################
# Update
####################

PREUPDATE()
{
	:
}

POSTUPDATE()
{
	# Stop fail2ban
	service fail2ban stop

	# Run config script file
	run_config_script

	# Make iptables persistent
	save_current_iptables

	# Start fail2ban
	service fail2ban start
}


####################
# Backup / Restore
####################

BACKUP()
{
	# Get backup path from main.sh
	backup_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"

	# Save the current state of the iptables
}

RESTORE()
{
	# Get restore path from main.sh
	restore_path=$1

	# Remove this line when implemented
	echo "Not implemented  $FUNCNAME in $service_name"

        # Restore the past state of the iptables
}


####################
# Main
####################

case "$1" in
    PREINSTALL)
        PREINSTALL
    ;;
    POSTINSTALL)
	POSTINSTALL
    ;;
    PREUPDATE)
	PREUPDATE
    ;;
    POSTUPDATE)
	POSTUPDATE
    ;;
    BACKUP)
        BACKUP $2
    ;;
    RESTORE)
        RESTORE $2
    ;;
    *)
	echo "

****** Service - $service_name - Main menu *****

Usage : ./$service_name.sh action

Veuillez choisir une action :

PREINSTALL
POSTINSTALL

PREUPDATE
POSTUPDATE

BACKUP
RESTORE
"
esac

exit 0
