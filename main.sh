#!/bin/bash

##################################################
#
# Main installation script
#
##################################################

####################
# Local variables
####################

services_folder='services/'
tmp_folder='tmp/'
backup_path='backup/'

vm_action=$1
vm_num=$2


####################
# Usage
####################

print_usage()
{
	echo "
Usage : ./main.sh action num

action :
	INSTALL
	UPDATE
	BACKUP
	RESTORE

num :
	Virtual machine number (see config folder)
"
}


####################
# Check input parameters
####################

if [ $# -ne 2 ]
then
	print_usage
	exit 0
else

	case "$vm_action" in
	    INSTALL)
	        action=INSTALL
	    ;;
	    UPDATE)
	        action=UPDATE
	    ;;
	    BACKUP)
	        action=BACKUP
	    ;;
	    RESTORE)
	        action=RESTORE
	    ;;
	    *)
	        print_usage
		exit 0
	esac
fi

echo -e "\n\n\n------------------------------"
echo -e "Virtual machine N°$vm_num - $vm_action"
echo -e "------------------------------\n\n\n"


####################
# Config files
####################

# All new variables will be added to ENV variables
set -a

# Import global config files
source config/vm_all/config.sh
source config/vm_all/private.sh

# Import vm specific config files
source config/vm_$2/config.sh
source config/vm_$2/private.sh

# All new variables will NOT be added to ENV variables
set +a


####################
# Load services
####################

# Load global services
services_to_call="$ROOT_SERVICES $LOCAL_SERVICES"

echo "Services to call : $services_to_call"
echo
echo


####################
# Replace all global variables in folder by name
####################
replace_var_in_folder_by_name() # $1 folder, $2 var_name
{
	folder=$1
	var_name=$2

	# Get variable text and value from name
	variableText="__"$var_name"__"
	variableValue=${!var_name}

	# Find all files in the (sub)folder(s) and replace variableName by value
	find $folder -name \* -type f -exec sed -i "s&$variableText&$variableValue&g" {} \;
}


####################
# Replace all global variables in folder
####################
replace_all_global_var_in_folder() # $1 folder
{
	folder=$1

	# Get all global variables and loop at them
	for global_var in $(compgen -e); do

		# Replace variable name by value in this folder
		replace_var_in_folder_by_name $folder $global_var
	done
}


####################
# General folder copy method
####################
copy_folder() # $1 source, $2 destination
{
	source_folder=$1
	destination_folder=$2

   if [ -d $source_folder ]
   then
      if [ ! -d $destination_folder ]
      then
	      # Create destination folder if not exist
	      mkdir $destination_folder
      fi

	   # Copy files from service folder to temp folder
	   cp -r $source_folder/. $destination_folder

   else
   	echo "Warning - Copy ignored - Folder not found: $source_folder "
   fi
}


####################
# Execute actions
####################

if [[ $action == "INSTALL" ]] || [[ $action == "UPDATE" ]]
then

	# Update apt-get before install or update anything
	apt-get update

	####################
	# Generate tmp folder with all services and replace global variables
	####################
	# Copy files from services folder to temp folder
	copy_folder $services_folder $tmp_folder

	# Replace global variables in tmp folder
	replace_all_global_var_in_folder $tmp_folder


	####################
	# PRE install/update
	####################
	echo -e "\n\n----------"
	echo -e "PRE$action"
	echo -e "----------"

	for scriptName in $services_to_call
	do
		# Print current service
		echo -e "\n$scriptName"
		echo -e "**********"

		service_script="$tmp_folder$scriptName/$scriptName.sh"
		$service_script PRE${action}
	done


	####################
	# install/update
	####################
	echo -e "\n\n----------"
	echo -e "$action"
	echo -e "----------"

	for scriptName in $services_to_call
	do
		# Print current service
		echo -e "\n$scriptName"
		echo -e "**********"

		#Get service folder with data to copy
		service_folder="$tmp_folder$scriptName/system_root/"

		# Copy service into root
		copy_folder $service_folder /
	done


	####################
	# POST install/update
	####################
	echo -e "\n\n----------"
	echo -e "POST$action"
	echo -e "----------"

	for scriptName in $services_to_call
	do
		# Print current service
		echo -e "\n$scriptName"
		echo -e "**********"

		service_script="$tmp_folder$scriptName/$scriptName.sh"
		$service_script POST${action}
	done

	#Remove tmp_folder
	if [ -d $tmp_folder ]
	then
		rm -rf $tmp_folder
	fi


elif [[ $action == "BACKUP" ]]
then
	####################
	# backup
	####################
	echo -e "\n\n----------"
	echo -e "$action"
	echo -e "----------"


	# Build backup folder path
	date_now=$(date --rfc-3339=date)
	vm_folder="vm_$vm_num/"
	full_backup_path="$backup_path$vm_folder$date_now"

	# Create backup directory
	mkdir -p $full_backup_path/


	for scriptName in $services_to_call
	do
		# Print current service
		echo -e "\n$scriptName"
		echo -e "**********"
            
		
		# Create backup folder for each services
		service_backup_folder="$full_backup_path/$scriptName/"
		mkdir -p $service_backup_folder

		# Call each services and give backup path
		service_script="services/$scriptName/$scriptName.sh"
		$service_script ${action} ${service_backup_folder}
	done

elif [[ $action == "RESTORE" ]]
then

	####################
	# restore
	####################
	echo -e "\n\n----------"
	echo -e "$action"
	echo -e "----------"


	# Get backup folder
	# TODO
	#$full_backup_path=?????


	for scriptName in $services_to_call
	do
		# Print current service
		echo -e "\n$scriptName"
		echo -e "**********"
            
		
		# Get backup folder for each services
		service_backup_folder="$full_backup_path/$scriptName/"
		
		# Call each services and give backup path
		service_script="services/$scriptName/$scriptName.sh"
		$service_script ${action} ${service_backup_folder}
	done

else

	echo "Error in main.sh"


fi

echo -e "\n\n----------"
echo -e "DONE $action VM_$2"
echo -e "----------"

exit 0

