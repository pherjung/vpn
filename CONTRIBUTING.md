Liste des todo
===================================

Pour les motivés à taper du code pour l'association, voici la liste des travaux en cours. Vous y trouverez tous les projets sur lesquels nous travaillons actuellement, leur responsable ainsi que les membres participants. N'hésitez pas à contacter les membres responsables pour participer ou pour plus d'informations.

Merci pour votre contribution !

La liste actualisée se trouve sur : https://swissneutralnet.framaboard.org


## SSH
- Responsable           :
- Membres participants  : Samuel Aymon

- Description           :
Serveur SSH
Generation clés
Partage de la clé publique
Ajout des clés public des administrateurs

- Tâches à effectuer    :



## MariaDB

- Responsable		: Sven Rouvinez
- Membres participants	:

- Description		:
Mise en place d'une base de donnée

- Tâches à effectuer	:



## iptables / firewall

- Responsable		: Samuel Aymon
- Membres participants	: 

- Description		:
Mettre en place des règles iptables IPv4 & IPv6. Les règles doivent être générées de manière automatisées en fonction des services présents dans les fichiers de configuration généraux.

- Tâches à effectuer	:



## Wiki / site web

- Responsable		: 
- Membres participants	: 

- Description		:
Mise en place d'un wiki et d'un site web.

- Tâches à effectuer	:



## Git

- Responsable		: 
- Membres participants	: 

- Description		:
Mise en place d'un git (gitLab, GOX)

- Tâches à effectuer	:



## Monitoring

- Responsable		: 
- Membres participants	: 

- Description		:
Trouver une solution de monitoring. (Monitorx, graphana, monite)

- Tâches à effectuer	:



## OpenVPN

- Responsable		: 
- Membres participants	: 

- Description		:
Mise en place du serveur VPN.

- Tâches à effectuer	:



## LDAP

- Responsable		: Josue
- Membres participants	: 

- Description		:
Mise en place du serveur LDAP.

- Tâches à effectuer	:



## Normalisation des scripts

- Responsable		: Josue
- Membres participants	: Samuel Aymon

- Description		:
Normalisation des scripts qui seront écrits dans le git.

- Tâches à effectuer	:



## Bind DNS

- Responsable		: Florian
- Membres participants	: 

- Description		:
Mise en place d'un serveur DNS.

- Tâches à effectuer	:



## Forum discorse

- Responsable		: 
- Membres participants	: 

- Description		:
Mise en place d'un formum.

- Tâches à effectuer	:



## Dolibarr

- Responsable		: 
- Membres participants	: 

- Description		:


- Tâches à effectuer	:



## Postfix

- Responsable		: 
- Membres participants	: 

- Description		:


- Tâches à effectuer	:



## Reverseproxy nginx

- Responsable		: 
- Membres participants	: 

- Description		:


- Tâches à effectuer	:



